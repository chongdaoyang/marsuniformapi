﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cdy.Tag.Message
{
    /// <summary>
    /// 消息类型，0:报警,1:通知消息
    /// </summary>
    public enum MessgeType
    {
        /// <summary>
        /// 报警
        /// </summary>
        Alarm,
        /// <summary>
        /// 通知消息
        /// </summary>
        InfoMessage
    }

    /// <summary>
    /// 
    /// </summary>
    public enum MessageOperate
    {
        /// <summary>
        /// 新建
        /// </summary>
        Create,
        /// <summary>
        /// 恢复
        /// </summary>
        Restore,
        /// <summary>
        /// 确认
        /// </summary>
        Ack,
        /// <summary>
        /// 删除
        /// </summary>
        Delete,
        /// <summary>
        /// 增加注释
        /// </summary>
        Note
    }

    /// <summary>
    /// 
    /// </summary>
    public enum MessageTriggerType
    {
        Keep,
        Trigger
    }

    /// <summary>
    /// 0:提示信息,1:预警,2:一般,3:重要,4:紧急,5:非常紧急
    /// </summary>
    public enum AlarmLevel
    {
        /// <summary>
        /// 提示信息
        /// </summary>
        Info = 0,
        /// <summary>
        /// 预警
        /// </summary>
        EarlyWarning = 1,
        /// <summary>
        /// 一般
        /// </summary>
        Normal = 2,
        /// <summary>
        /// 重要
        /// </summary>
        Critical = 3,
        /// <summary>
        /// 紧急
        /// </summary>
        Urgency = 4,

        /// <summary>
        /// 非常紧急
        /// </summary>
        VeryUrgency = 5,
        /// <summary>
        /// 自定义级别
        /// </summary>
        Custom1 = 6,
        Custom2 = 7,
        Custom3 = 8,
        Custom4 = 9,
        Custom5 = 10,
        Custom6 = 11,
        Custom7 = 12,
        Custom8 = 13,
        Custom9 = 14,
        Custom10 = 15,
        Custom11 = 16,
        Custom12 = 17,
        Custom13 = 18,
        Custom14 = 19,
        Custom15 = 20,
        Custom16 = 21

    }

    /// <summary>
    /// 
    /// </summary>
    public abstract class Message
    {
        /// <summary>
        /// ID编号
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 服务器
        /// </summary>
        public string Server { get; set; } = "";

        /// <summary>
        /// 生产者变量
        /// </summary>
        public string SourceTag { get; set; } = "";

        /// <summary>
        /// 消息产生时间
        /// </summary>
        public DateTime CreateTime { get; set; }



        /// <summary>
        /// 
        /// </summary>
        public string MessageBody
        { get; set; } = "";

        /// <summary>
        /// 消息类型，0:报警,1:通知消息
        /// </summary>
        public abstract MessgeType Type { get; }



        /// <summary>
        /// 附加字段1
        /// </summary>
        public string AppendContent1 { get; set; } = "";

        /// <summary>
        /// 附加字段2
        /// </summary>
        public string AppendContent2 { get; set; } = "";


        /// <summary>
        /// 附加字段3
        /// </summary>
        public string AppendContent3 { get; set; } = "";


        /// <summary>
        /// 
        /// </summary>
        public MessageOperate Operate { get; set; }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder re = new StringBuilder();

            re.Append((int)Type).Append("^").Append(Id).Append("^").Append(Server).Append("^").Append(SourceTag).Append("^").Append(CreateTime.Ticks).Append("^").Append(MessageBody).Append("^");
            re.Append(AppendContent1).Append("^").Append(AppendContent2).Append("^").Append(AppendContent3).Append("^").Append((byte)Operate);

            return re.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="val"></param>
        public virtual Message LoadFromString(string[] val)
        {
            Id = long.Parse(val[1]);
            Server = val[2];
            SourceTag = val[3];
            CreateTime = DateTime.FromBinary(long.Parse(val[4]));
            MessageBody = val[5];
            AppendContent1 = val[6];
            AppendContent2 = val[7];
            AppendContent3 = val[8];
            Operate = (MessageOperate)(byte.Parse(val[9]));
            return this;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static Message LoadFromString(string val)
        {
            string[] sval = val.Split(new char[] { '^' });
            if (sval[0] == "0")
            {
                return new AlarmMessage().LoadFromString(sval);
            }
            else
            {
                return new InfoMessage().LoadFromString(sval);
            }
        }

    }

    /// <summary>
    /// 提示消息
    /// </summary>
    public class InfoMessage : Message
    {
        /// <summary>
        /// 数据快照
        /// </summary>
        public string Snapshot { get; set; }

        public override string ToString()
        {
            var re = base.ToString();
            StringBuilder sb = new StringBuilder();
            sb.Append(Snapshot);
            return re + "^" + sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="val"></param>
        public override Message LoadFromString(string[] val)
        {
            base.LoadFromString(val);
            Snapshot = val[10];
            return this;
        }

        /// <summary>
        /// 消息类型，0:报警,1:通知消息
        /// </summary>
        public override MessgeType Type => MessgeType.InfoMessage;
    }


    /// <summary>
    /// 报警消息
    /// </summary>
    public class AlarmMessage : Message
    {
        /// <summary>
        /// 消息类型，0:报警,1:通知消息
        /// </summary>
        public override MessgeType Type => MessgeType.Alarm;

        /// <summary>
        /// 报警级别,0:提示信息,1:预警,2:一般,3:重要,4:紧急,5:非常紧急
        /// </summary>
        public AlarmLevel AlarmLevel { get; set; }

        /// <summary>
        /// 报警值
        /// </summary>
        public string AlarmValue { get; set; } = "";

        /// <summary>
        /// 报警时的报警条件
        /// </summary>
        public string AlarmCondition { get; set; } = "";

        /// <summary>
        /// 关联变量
        /// </summary>
        public string LinkTag { get; set; } = "";

        /// <summary>
        /// 消息触发类型，
        /// 触发模式没有消息恢复过程
        /// 保持模可恢复
        /// </summary>
        public MessageTriggerType TriggerType { get; set; } = MessageTriggerType.Keep;

        /// <summary>
        /// 恢复时间
        /// </summary>
        public DateTime RestoreTime { get; set; }

        /// <summary>
        /// 恢复值
        /// </summary>
        public string RestoreValue { get; set; } = "";

        /// <summary>
        /// 恢复类型,0:报警恢复,1:报警升级、降级,2:报警撤销,3:手动恢复
        /// </summary>
        public byte RestoreType { get; set; }


        /// <summary>
        /// 确认时间
        /// </summary>
        public DateTime AckTime { get; set; }

        /// <summary>
        /// 确认人
        /// </summary>
        public string AckUser { get; set; }

        /// <summary>
        /// 确认内容
        /// </summary>
        public string AckMessage { get; set; }

        /// <summary>
        /// 数据快照
        /// </summary>
        public string Snapshot { get; set; }

        /// <summary>
        /// 报警抑制
        /// </summary>
        public bool IsRestrain { get; set; }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var re = base.ToString();
            StringBuilder sb = new StringBuilder();
            sb.Append((int)AlarmLevel).Append("^").Append(AlarmValue).Append("^").Append(AlarmCondition).Append("^").Append(LinkTag).Append("^").Append(RestoreTime.Ticks).Append("^").Append(RestoreValue).Append("^").Append(RestoreType).Append("^").Append(AckTime.Ticks).Append("^").Append(AckUser).Append("^").Append(AckMessage).Append("^").Append(Snapshot).Append("^").Append(IsRestrain);
            return re + "^" + sb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="val"></param>
        public override Message LoadFromString(string[] val)
        {
            base.LoadFromString(val);
            AlarmLevel = (AlarmLevel)int.Parse(val[10]);
            AlarmValue = val[11];
            AlarmCondition = val[12];
            LinkTag = val[13];
            RestoreTime = DateTime.FromBinary(long.Parse(val[14]));
            RestoreValue = val[15];
            RestoreType = byte.Parse(val[16]);
            if(val.Length > 17)
            {
                AckTime = DateTime.FromBinary(long.Parse(val[17]));
                AckUser = val[18];
                AckMessage = val[19];
            }

            if(val.Length > 20)
            {
                Snapshot = val[20];
            }
            
            if(val.Length>21)
            {
                IsRestrain = bool.Parse(val[21]);
            }

            return this;
        }



    }


}
