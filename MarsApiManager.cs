﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsUniformApi
{
    /// <summary>
    /// Mars API 管理
    /// </summary>
    public class MarsApiFactory
    {
        public static MarsApiFactory Factory = new MarsApiFactory();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public MarsApi GetApi(string ip,string username,string password)
        {
            return new MarsApi(ip, username, password);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ip">Ip</param>
        /// <param name="username">用户名</param>
        /// <param name="password">密码</param>
        /// <param name="enableCollector">使能采集API</param>
        /// <param name="enableComsumer">使能业务API</param>
        /// <param name="enbaleManager">使能管理API</param>
        /// <returns></returns>
        public MarsApi GetApi(string ip,string username,string password,bool enableCollector,bool enableComsumer,bool enbaleManager)
        {
            var api = new MarsApi();
            if(enableCollector)
            {
                api.EnableCollector(true, ip, username, password);
            }
            if(enableComsumer)
            {
                api.EnableComsumer(true, ip, username, password);
            }
            if(enbaleManager)
            {
                api.EnableManager(true, ip, username, password);
            }
            return api;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="collectorUserName">采集API 用户名</param>
        /// <param name="collectorPassword">采集API 密码</param>
        /// <param name="comsumerUsername">业务API 用户名</param>
        /// <param name="comsumerPassword">业务API 密码</param>
        /// <param name="managerUsername">管理API 用户名</param>
        /// <param name="managerPassword">管理API 密码</param>
        /// <returns></returns>
        public MarsApi GetApi(string ip, string collectorUserName,string collectorPassword, string comsumerUsername,string comsumerPassword,string managerUsername,string managerPassword)
        {
            var api = new MarsApi();
            api.EnableCollector(true, ip, collectorUserName, collectorPassword);
            api.EnableComsumer(true, ip, comsumerUsername, comsumerPassword);
            api.EnableManager(true, ip, managerUsername, managerPassword);
            return api;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ip">Ip地址</param>
        /// <param name="username">用户名</param>
        /// <param name="password">密码</param>
        /// <param name="enableCollector">使能采集API</param>
        /// <param name="collectorPort">采集API端口</param>
        /// <param name="enableComsumer">使能业务API</param>
        /// <param name="comsumerPort">采集API端口</param>
        /// <param name="enbaleManager">使能管理API</param>
        /// <param name="managerPort">管理API端口</param>
        /// <returns></returns>
        public MarsApi GetApi(string ip, string username, string password, bool enableCollector,int collectorPort, bool enableComsumer,int comsumerPort, bool enbaleManager,int managerPort)
        {
            var api = new MarsApi();
            if (enableCollector)
            {
                api.EnableCollector(true, ip, username, password, collectorPort);
            }
            if (enableComsumer)
            {
                api.EnableComsumer(true, ip, username, password, comsumerPort);
            }
            if (enbaleManager)
            {
                api.EnableManager(true, ip, username, password, managerPort);
            }
            return api;
        }

    }
}
