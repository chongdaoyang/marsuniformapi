﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MarsUniformApiExample
{
    /// <summary>
    /// DevelopApiTest.xaml 的交互逻辑
    /// </summary>
    public partial class DevelopApiTest : UserControl
    {
        //DBDevelopClientWebApi.WebDevelopApiClient mHelper;
        private string mCurrentDatabase = "";
        private string mTagGroup = "";

        public MarsUniformApi.MarsApi Helper { get; set; }

        public DevelopApiTest()
        {
            InitializeComponent();
            //mHelper = new DBDevelopClientWebApi.WebDevelopApiClient();
        }


        private void loginb_Click(object sender, RoutedEventArgs e)
        {
            //mHelper.Server = serverIp.Text;
            //mHelper.Login("Admin", "Admin");
        }

        private void getTag_Click(object sender, RoutedEventArgs e)
        {
            int count = 0;
            var tags = Helper.GetTagByGroup(mCurrentDatabase, mTagGroup, 0, out count);
            if (tags != null)
            {
                taglist.ItemsSource = tags.Select(ee => ee.Item1.Name).ToList();
            }
        }

        private void getTagGroup_Click(object sender, RoutedEventArgs e)
        {
            List<string> ltmp = new List<string>();
            ltmp.Add("");
            var grps = Helper.GetTagGroup(mCurrentDatabase);
            if (grps != null)
            {
                foreach (var vv in grps)
                {
                    if (!string.IsNullOrEmpty(vv.Parent))
                    {
                        ltmp.Add(vv.Parent + "." + vv.Name);
                    }
                    else
                    {
                        ltmp.Add(vv.Name);
                    }
                }
            }
            groupList.ItemsSource = ltmp;

        }

        private void getDatabase_Click(object sender, RoutedEventArgs e)
        {
            var vdd = Helper.QueryDatabase();
            if (vdd != null)
            {
                databaseList.ItemsSource = vdd.Select(ee => ee.Name);
            }
        }

        private void databaseList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            mCurrentDatabase = databaseList.SelectedItem.ToString();
        }

        private void groupList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            mTagGroup = groupList.SelectedItem.ToString();
        }

        private void remove_sel_db_Click(object sender, RoutedEventArgs e)
        {
            Helper.RemoveDatabase(mCurrentDatabase);
        }

        private void getpermssion(object sender, RoutedEventArgs e)
        {
            Helper.GetDatabasePermission(mCurrentDatabase);
        }

        private void getdatabaseuser_Click(object sender, RoutedEventArgs e)
        {
            var user = Helper.GetDatabaseUserByGroup("", mCurrentDatabase);
        }

        private void getsystemuser_Click(object sender, RoutedEventArgs e)
        {
            var user = Helper.GetUsers();
        }

        private void getCurrentsystemuser_Click(object sender, RoutedEventArgs e)
        {
            var info = Helper.GetCurrentUserConfig();
        }
    }
}
