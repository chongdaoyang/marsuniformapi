﻿using MarsUniformApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MarsUniformApiExample
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private MarsApi mClient;
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Login_Click(object sender, RoutedEventArgs e)
        {
            mClient = MarsUniformApi.MarsApiFactory.Factory.GetApi(server.Text, user.Text, pass.Text);
            (comsumerapi.DataContext as HighSpeedApiTestViewModel).Client = mClient;
            driverapi.Proxy = mClient;
            mamangerapi.Helper = mClient;
            mClient.Connect();
            mClient.Login();
            if(mClient.IsLogin())
            {
                MessageBox.Show("登录成功!");
            }
            else
            {
                MessageBox.Show("登录失败!");
            }
            
        }

        private void Logout_Click(object sender, RoutedEventArgs e)
        {
            (comsumerapi.DataContext as HighSpeedApiTestViewModel).Client = null;
            driverapi.Proxy = null;
            mamangerapi.Helper = null;

            mClient.Logout();
            mClient.Dispose();
            mClient=null;
        }
    }
}
